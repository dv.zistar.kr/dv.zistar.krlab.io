// JavaScript Document

$(document).ready(function() {
	
	var mySwiper = new Swiper ('.swiper-container', {
		//페이지버튼으로 사용할 클래스 지정
		pagination: '.swiper-pagination',
		//슬라이드 방향
		direction: 'vertical',
		//한 화면에 보일 슬라이더의 갯수 
		slidesPerView: 1,
		//페이지버튼 클릭으로 이동
		paginationClickable: true,
		//마우스휠로 이동
		mousewheelControl: true
	});
		
	$('.visual').slick({
		autoplay:true, //자동실행
		autoplaySpeed:4000, //자동실행 속도 (인터벌)
		speed:600, //애니메이션 속도
		arrows:false, //양쪽 화살표
		swipe:false, //마우스 드래그 또는 모바일에서 손으로 쓸어넘기는 기능
		pauseOnFocus:false, //포커스가 갔을떄 멈춤기능
		pauseOnHover:false,	//마우스 호버시 멈춤
		dots:true  
   });
   
});